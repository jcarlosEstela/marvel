//
//  TestsRestClient.swift
//  MarvelTests
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation
@testable import Marvel

class TestsRestClient: RestClient {
    
    private var stubResponses: [String: String] = [:]
    
    func addStubResponsePath(_ path: String, forUrl url: String) {
        stubResponses[url] = path
    }
    
    func request<QueryParam: Encodable, Output: Decodable>(
        url: String,
        method: HTTPMethod,
        query: QueryParam,
        completion: @escaping (Result<Output, Error>) -> Void
    ) {
        guard
            let jsonFilePath = stubResponses[url],
            let data = file(path: jsonFilePath, type: "json")
        else {
            return completion(.failure(RestClientError.unknown))
        }
        do {
            try completion(.success(JSONDecoder().decode(Output.self, from: data)))
        } catch {
            print(error)
            completion(.failure(RestClientError.decodingError))
        }
    }
}

private extension TestsRestClient {
    
    func file(path: String, type: String) -> Data? {
        guard let path = Bundle(for: TestsRestClient.self).path(forResource: path, ofType: type) else { return nil }
        return try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
    }
}
