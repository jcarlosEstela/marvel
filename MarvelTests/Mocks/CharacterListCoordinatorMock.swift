//
//  CharacterListCoordinatorMock.swift
//  MarvelTests
//
//  Created by Jose Carlos Estela Anguita on 10/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation
@testable import Marvel

struct CharacterListCoordinatorMock: CharacterListCoordinatorProtocol {
    
    func goToCharacterDetail(_ character: CharacterViewModel) {
        
    }
}
