//
//  CharacterListViewMock.swift
//  MarvelTests
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation
@testable import Marvel

final class CharacterListViewMock: CharacterListView {
    
    var presenter: CharacterListPresenterProtocol
    var spyShowCharacters: [CharacterViewModel]?
    var spyAddCharacters: [CharacterViewModel]?
    var spyShowError: (title: String, description: String?)?
    
    init(presenter: CharacterListPresenterProtocol) {
        self.presenter = presenter
    }
    
    func showPaginationLoading() {
        
    }
    
    func dismissPaginationLoading() {
        
    }
    
    func showCharacters(_ characters: [CharacterViewModel]) {
        spyShowCharacters = characters
    }
    
    func addCharacters(_ characters: [CharacterViewModel]) {
        spyAddCharacters = characters
    }
    
    func showAlert(title: String, description: String?, accept: AlertAction?, cancel: AlertAction?) {
        spyShowError = (title: title, description: description)
    }
    
    func showLoading() {
        
    }
    
    func dismissLoading() {
        
    }
}
