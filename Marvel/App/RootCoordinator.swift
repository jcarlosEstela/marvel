//
//  RootCoordinator.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 08/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

final class RootCoordinator: Coordinator {
    
    weak var navigationController: UINavigationController?
    private var characterListCoordinator: CharacterListCoordinator
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.characterListCoordinator = CharacterListCoordinator(navigationController: navigationController)
    }
    
    func start() {
        characterListCoordinator.start()
    }
}
