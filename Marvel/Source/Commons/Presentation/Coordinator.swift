//
//  Coordinator.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 08/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController? { get }
    func start()
}
