//
//  URLImageView.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

final class URLImageView: UIImageView {
    
    private var url: URL?
    
    func setImage(fromUrl url: String) {
        guard let url = URL(string: url) else { return }
        self.url = url
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: url)
            guard let image = data.flatMap(UIImage.init) else {
                return DispatchQueue.main.async {
                    self.image = nil
                }
            }
            DispatchQueue.main.async {
                if self.url?.absoluteString == url.absoluteString {
                    self.image = image
                }
            }
        }
    }
}
