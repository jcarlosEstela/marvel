//
//  AlertPresentationCapable.swift
//  Jest
//
//  Created by José Carlos Estela Anguita on 15/05/2020.
//

import UIKit

struct AlertAction {
    
    let title: String
    let action: (() -> Void)?
    
    init(title: String, action: (() -> Void)? = nil) {
        self.title = title
        self.action = action
    }
}

protocol AlertPresentationCapable {
    func showAlert(title: String, description: String?, accept: AlertAction?, cancel: AlertAction?)
}

extension AlertPresentationCapable where Self: UIViewController {
    
    func showAlert(title: String, description: String?, accept: AlertAction?, cancel: AlertAction?) {
        let alert = UIAlertController(title: title, message: description, preferredStyle: .alert)
        let acceptAction = accept.map { action in
            UIAlertAction(title: action.title, style: .default, handler: { _ in action.action?() })
        }
        let cancelAction = cancel.map { action in
            UIAlertAction(title: action.title, style: .cancel, handler: { _ in action.action?() })
        }
        [acceptAction, cancelAction].compactMap({ $0 }).forEach(alert.addAction)
        present(alert, animated: true, completion: nil)
    }
}
