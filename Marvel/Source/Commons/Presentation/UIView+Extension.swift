//
//  UIView+Extension.swift
//  Marvel
//
//  Created by Jose Carlos Estela Anguita on 10/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

extension UIView {
    
    struct SubviewMargin {
        let top: CGFloat
        let bottom: CGFloat
        let left: CGFloat
        let right: CGFloat
        
        static func zero() -> SubviewMargin {
            return SubviewMargin(top: 0, bottom: 0, left: 0, right: 0)
        }
    }
    
    func addSubview(_ view: UIView, margin: SubviewMargin) {
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        rightAnchor.constraint(equalTo: view.rightAnchor, constant: margin.right).isActive = true
        view.leftAnchor.constraint(equalTo: leftAnchor, constant: margin.left).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor, constant: margin.top).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: margin.bottom).isActive = true
    }
}
