//
//  Contants.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 06/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

enum Constants {
    enum Api {
        enum Authentication {
            static let key = "92d76235bb99022666fdbaa30ab6092a"
            static let secret = "ff11cfb139fe624034d9667e948dcb2d39467b9f"
        }
        static let baseUrl = "https://gateway.marvel.com:443/v1/public/"
    }
}
