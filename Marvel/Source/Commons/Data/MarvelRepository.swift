//
//  MarvelRepository.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 06/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol MarvelRepository {
    func hash(withTimestamp timestamp: String) -> String
}

extension MarvelRepository {
    
    func hash(withTimestamp timestamp: String) -> String {
        let apiKey = Constants.Api.Authentication.key
        let apiSecret = Constants.Api.Authentication.secret
        return (timestamp + apiSecret + apiKey).md5()
    }
}
