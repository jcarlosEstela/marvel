//
//  StoriesDTO.swift
//  Marvel
//
//  Created by Jose Carlos Estela Anguita on 10/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct StoriesDTO: Decodable {
    let available: Int
    let collectionURI: String
    let items: [StoriesItemDTO]
    let returned: Int
}

struct StoriesItemDTO: Decodable {
    let resourceURI: String
    let name: String
    let type: String
}
