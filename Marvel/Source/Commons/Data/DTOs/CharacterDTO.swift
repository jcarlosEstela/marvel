//
//  CharacterDTO.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 06/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct CharacterListDTO: Decodable {
    let data: CharacterDataDTO
}

struct CharacterDataDTO: Decodable {
    let offset: Int
    let limit: Int
    let total: Int
    let count: Int
    let results: [CharacterDTO]
}

struct CharacterDTO: Decodable {
    let id: Int
    let name: String
    let characterDescription: String
    let thumbnail: ThumbnailDTO
    let comics: ComicsDTO
    let stories: StoriesDTO
    
    enum CodingKeys: String, CodingKey {
        case id, name, comics, stories, thumbnail
        case characterDescription = "description"
    }
}
