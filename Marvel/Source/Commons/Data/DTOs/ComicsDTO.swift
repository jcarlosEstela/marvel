//
//  ComicsDTO.swift
//  Marvel
//
//  Created by Jose Carlos Estela Anguita on 10/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct ComicsDTO: Decodable {
    let available: Int
    let collectionURI: String
    let items: [ComicsItemDTO]
    let returned: Int
}

struct ComicsItemDTO: Decodable {
    let resourceURI: String
    let name: String
}
