//
//  RestClient.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 05/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

enum HTTPMethod {
    case get
}

extension HTTPMethod: CustomStringConvertible {
    var description: String {
        switch self {
        case .get:
            return "GET"
        }
    }
}

enum RestClientError: LocalizedError {
    case unknown
    case decodingError
    case api(Error)
    
    var errorDescription: String? {
        switch self {
        case .unknown, .decodingError:
            return "Unknown error"
        case .api(let error):
            return error.localizedDescription
        }
    }
}

protocol RestClient {
    
    func request<QueryParam: Encodable, Output: Decodable>(
        url: String,
        method: HTTPMethod,
        query: QueryParam,
        completion: @escaping (Result<Output, Error>) -> Void
    )
}

struct DefaultRestClient: RestClient {
    
    func request<QueryParam: Encodable, Output: Decodable>(
        url: String,
        method: HTTPMethod,
        query: QueryParam,
        completion: @escaping (Result<Output, Error>) -> Void
    ) {
        guard let request = urlRequest(method: method.description, url: url, query: query) else { return completion(.failure(RestClientError.unknown)) }
        URLSession.shared.dataTask(with: request) { data, response, error in
            self.handleResponse(data: data, response: response, error: error, completion: completion)
        }.resume()
    }
}

private extension DefaultRestClient {
    
    func handleResponse<Output: Decodable>(data: Data?, response: URLResponse?, error: Error?, completion: @escaping (Result<Output, Error>) -> Void) {
        guard let responseData = data else { return handleResponseError(error, completion: completion) }
        handleSuccessResponse(withData: responseData, completion: completion)
    }
    
    func handleResponseError<Output: Decodable>(_ error: Error?, completion: @escaping (Result<Output, Error>) -> Void) {
        let error: RestClientError = error.map({ .api($0) }) ?? .unknown
        DispatchQueue.main.async {
            completion(.failure(error))
        }
    }
    
    func handleSuccessResponse<Output: Decodable>(withData data: Data, completion: @escaping (Result<Output, Error>) -> Void) {
        do {
            let output = try JSONDecoder().decode(Output.self, from: data)
            DispatchQueue.main.async {
                completion(.success(output))
            }
        } catch {
            DispatchQueue.main.async {
                completion(.failure(RestClientError.decodingError))
            }
        }
    }
    
    func urlRequest<QueryParam: Encodable>(method: String, url: String, query: QueryParam) -> URLRequest? {
        guard let url = URL(string: url) else { return nil }
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        urlComponents?.queryItems = query.queryItems()
        guard let urlWithParams = urlComponents?.url else { return URLRequest(url: url) }
        var request = URLRequest(url: urlWithParams)
        request.httpMethod = method
        return request
    }
}

private extension Encodable {
    
    func queryItems() -> [URLQueryItem]? {
        guard let data = try? JSONEncoder().encode(self), let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any], dictionary.keys.count > 0 else {
            return nil
        }
        return dictionary.compactMap {
            guard let stringParam = $0.value as? String else { return nil }
            return URLQueryItem(name: $0.key, value: stringParam)
        }
    }
}
