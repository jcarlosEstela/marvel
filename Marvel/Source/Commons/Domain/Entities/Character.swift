//
//  Character.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 06/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct Character {
    let id: Int
    let name: String
    let thumbnail: String
    let comics: [Comic]
    let stories: [Story]
}

extension Character: Equatable {
    
    static func == (lhs: Character, rhs: Character) -> Bool {
        return lhs.id == rhs.id
    }
}
