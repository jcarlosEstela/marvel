//
//  Story.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 10/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct Story {
    let name: String
}
