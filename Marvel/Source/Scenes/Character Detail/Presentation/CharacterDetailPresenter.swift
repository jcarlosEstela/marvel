//
//  CharacterDetailPresenter.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol CharacterDetailPresenterProtocol {
    var view: CharacterDetailView? { get }
    func viewDidLoad()
    func dismiss()
}

final class CharacterDetailPresenter {
    
    private let character: CharacterViewModel
    private let coordinator: CharacterDetailCoordinatorProtocol
    weak var view: CharacterDetailView?
    
    init(character: CharacterViewModel, coordinator: CharacterDetailCoordinatorProtocol) {
        self.character = character
        self.coordinator = coordinator
    }
}

extension CharacterDetailPresenter: CharacterDetailPresenterProtocol {
    
    func viewDidLoad() {
        view?.showCharacter(character)
    }
    
    func dismiss() {
        coordinator.dismiss()
    }
}
