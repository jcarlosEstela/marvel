//
//  CharacterDetailCoordinator.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

protocol CharacterDetailCoordinatorProtocol {
    func dismiss()
}

final class CharacterDetailCoordinator: Coordinator {
    
    weak var navigationController: UINavigationController?
    private let character: CharacterViewModel
    private weak var viewController: UIViewController?
    
    init(navigationController: UINavigationController?, character: CharacterViewModel) {
        self.navigationController = navigationController
        self.character = character
    }
    
    func start() {
        let presenter = CharacterDetailPresenter(character: character, coordinator: self)
        let view = CharacterDetailViewController(presenter: presenter)
        presenter.view = view
        view.modalPresentationStyle = .overCurrentContext
        view.modalTransitionStyle = .crossDissolve
        navigationController?.present(view, animated: true, completion: nil)
        viewController = view
    }
}

extension CharacterDetailCoordinator: CharacterDetailCoordinatorProtocol {
    
    func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }
}
