//
//  CharacterDetailViewController.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

protocol CharacterDetailView: class {
    func showCharacter(_ character: CharacterViewModel)
}

final class CharacterDetailViewController: UIViewController {
    
    // MARK: - Attributes
    
    private lazy var characterImage: URLImageView = {
        let image = URLImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.contentMode = .scaleAspectFill
        return image
    }()
    private lazy var characterTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .boldSystemFont(ofSize: 30)
        label.numberOfLines = 0
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    private lazy var detailStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 10
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var detailContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        return view
    }()
    
    private lazy var comicsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var storiesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    let presenter: CharacterDetailPresenterProtocol
    
    init(presenter: CharacterDetailPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "CharacterDetailViewController", bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        presenter.viewDidLoad()
    }
}

extension CharacterDetailViewController: CharacterDetailView {
    
    func showCharacter(_ character: CharacterViewModel) {
        characterImage.setImage(fromUrl: character.thumbnail)
        characterTitleLabel.text = character.name
        showComics(from: character)
        showStories(from: character)
    }
}

private extension CharacterDetailViewController {
    
    func setupView() {
        setupBackground()
        setupContainerView()
        setupImage()
        setupScrollView()
        setupStackView()
        setupCloseButton()
        setupTitle()
        setupComics()
        setupStories()
    }
    
    func setupContainerView() {
        view.addSubview(detailContainerView, margin: UIView.SubviewMargin(top: 40, bottom: 40, left: 20, right: 20))
        detailContainerView.layer.masksToBounds = true
        detailContainerView.layer.cornerRadius = 5.0
    }
    
    func setupBackground() {
        view.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
    func setupImage() {
        detailContainerView.addSubview(characterImage, margin: .zero())
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        detailContainerView.addSubview(background, margin: .zero())
    }
    
    func setupTitle() {
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.addSubview(characterTitleLabel, margin: UIView.SubviewMargin(top: 0, bottom: 0, left: 10, right: 10))
        background.backgroundColor = .clear
        detailStackView.addArrangedSubview(background)
    }
    
    func setupScrollView() {
        detailContainerView.addSubview(scrollView, margin: .zero())
    }
    
    func setupStackView() {
        scrollView.addSubview(detailStackView)
        detailStackView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        detailStackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 10).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: detailStackView.bottomAnchor, constant: 10).isActive = true
        detailStackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.8).isActive = true
    }
    
    func setupCloseButton() {
        let background = UIView()
        background.translatesAutoresizingMaskIntoConstraints = false
        background.backgroundColor = .clear
        detailStackView.addArrangedSubview(background)
        let button = UIButton()
        button.setTitle("Close", for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 15)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        background.addSubview(button)
        button.trailingAnchor.constraint(equalTo: background.trailingAnchor, constant: -10).isActive = true
        button.topAnchor.constraint(equalTo: background.topAnchor).isActive = true
        button.bottomAnchor.constraint(equalTo: background.bottomAnchor).isActive = true
        button.heightAnchor.constraint(equalToConstant: 60).isActive = true
    }
    
    func setupComics() {
        detailStackView.addArrangedSubview(comicsStackView)
    }
    
    func setupStories() {
        detailStackView.addArrangedSubview(storiesStackView)
    }
    
    func showComics(from character: CharacterViewModel) {
        guard character.comics.count > 0 else { return }
        showParticipation(of: character, type: \.comics, title: "Comics", on: comicsStackView)
    }
    
    func showStories(from character: CharacterViewModel) {
        guard character.stories.count > 0 else { return }
        showParticipation(of: character, type: \.stories, title: "Stories", on: storiesStackView)
    }
    
    func showParticipation(of character: CharacterViewModel, type keyPath: KeyPath<CharacterViewModel, [String]>, title: String, on stackView: UIStackView) {
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.textColor = .white
        let labels: [UILabel] = character[keyPath: keyPath].map({
            let label = UILabel()
            label.text = "* " + $0
            label.font = .systemFont(ofSize: 16)
            label.textColor = .white
            label.numberOfLines = 0
            return label
        })
        stackView.addArrangedSubview(titleLabel)
        labels.forEach(stackView.addArrangedSubview)
        stackView.setCustomSpacing(10, after: titleLabel)
    }
    
    @objc func closeAction() {
        presenter.dismiss()
    }
}
