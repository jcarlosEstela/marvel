//
//  LoadCharactersByNameUseCase.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 07/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct LoadCharactersByNameUseCaseOutput: LoadCharactersUseCaseOutputProtocol {
    let characters: [Character]
    let isPaginationEnabled: Bool
}

struct LoadCharactersByNameUseCaseInput {
    let offset: Int
    let limit: Int = 20
    let nameStartsWith: String
}

protocol LoadCharactersByNameUseCaseProtocol {
    func load(with input: LoadCharactersByNameUseCaseInput, completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void)
}

struct LoadCharactersByNameUseCase: LoadCharactersByNameUseCaseProtocol {
    
    let repository: LoadCharactersRepository
    
    func load(with input: LoadCharactersByNameUseCaseInput, completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void) {
        repository.get(nameStartsWith: input.nameStartsWith, from: input.offset, limit: input.limit) { result in
            switch result {
            case .success(let characters):
                let isPaginationEnabled = characters.count == input.limit
                completion(.success(LoadCharactersByNameUseCaseOutput(characters: characters, isPaginationEnabled: isPaginationEnabled)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
