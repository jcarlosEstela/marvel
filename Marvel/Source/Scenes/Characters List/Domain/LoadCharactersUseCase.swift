//
//  LoadCharactersUseCase.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 06/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol LoadCharactersUseCaseOutputProtocol {
    var characters: [Character] { get }
    var isPaginationEnabled: Bool { get }
}

struct LoadCharactersUseCaseOutput: LoadCharactersUseCaseOutputProtocol {
    let characters: [Character]
    let isPaginationEnabled: Bool
}

struct LoadCharactersUseCaseInput {
    let offset: Int
    let limit: Int = 20
}

protocol LoadCharactersUseCaseProtocol {
    func load(with input: LoadCharactersUseCaseInput, completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void)
}

struct LoadCharactersUseCase: LoadCharactersUseCaseProtocol {
    
    let repository: LoadCharactersRepository
    
    func load(with input: LoadCharactersUseCaseInput, completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void) {
        repository.get(from: input.offset, limit: input.limit) { result in
            switch result {
            case .success(let characters):
                let isPaginationEnabled = characters.count == input.limit
                completion(.success(LoadCharactersUseCaseOutput(characters: characters, isPaginationEnabled: isPaginationEnabled)))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
