//
//  LoadCharactersRepository.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 05/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol LoadCharactersRepository {
    func get(from offset: Int, limit: Int, completion: @escaping (Result<[Character], Error>) -> Void)
    func get(nameStartsWith: String, from offset: Int, limit: Int, completion: @escaping (Result<[Character], Error>) -> Void)
}
