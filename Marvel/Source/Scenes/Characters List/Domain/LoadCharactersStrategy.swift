//
//  LoadCharactersStrategy.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 08/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol LoadCharactersUseCaseStrategy {
    func loadCharacters(completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void)
}

struct LoadAllCharactersUseCaseStrategy: LoadCharactersUseCaseStrategy {
    
    let offset: Int
    let useCase: LoadCharactersUseCaseProtocol
    
    func loadCharacters(completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void) {
        useCase.load(with: LoadCharactersUseCaseInput(offset: offset), completion: completion)
    }
}

struct LoadCharactersByNameUseCaseStrategy: LoadCharactersUseCaseStrategy {
    
    let term: String
    let offset: Int
    let useCase: LoadCharactersByNameUseCaseProtocol
    
    func loadCharacters(completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void) {
        useCase.load(with: LoadCharactersByNameUseCaseInput(offset: offset, nameStartsWith: term), completion: completion)
    }
}

struct LoadCharactersUseCaseStrategyFactory {
    
    let loadAllCharactersUseCase: LoadCharactersUseCaseProtocol
    let loadCharactersByNameUseCase: LoadCharactersByNameUseCaseProtocol
    
    func get(withTerm term: String?, offset: Int) -> LoadCharactersUseCaseStrategy {
        guard let unwrappedTerm = term, !unwrappedTerm.isEmpty else {
            return LoadAllCharactersUseCaseStrategy(
                offset: offset,
                useCase: loadAllCharactersUseCase
            )
        }
        return LoadCharactersByNameUseCaseStrategy(
            term: unwrappedTerm,
            offset: offset,
            useCase: loadCharactersByNameUseCase
        )
    }
}
