//
//  CharacterListViewController.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 05/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

protocol CharacterListView: class, AlertPresentationCapable, LoadingPresentationCapable {
    var presenter: CharacterListPresenterProtocol { get }
    func showPaginationLoading()
    func dismissPaginationLoading()
    func showCharacters(_ characters: [CharacterViewModel])
    func addCharacters(_ characters: [CharacterViewModel])
}

class CharacterListViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.dataSource = self
            tableView.delegate = self
        }
    }
    @IBOutlet private weak var paginationView: UIView! {
        didSet {
            paginationView.isHidden = true
        }
    }
    @IBOutlet private weak var searchBar: UISearchBar! {
        didSet {
            searchBar.delegate = self
        }
    }
    @IBOutlet private weak var paginationActivityView: UIActivityIndicatorView!
    @IBOutlet private weak var searchBarActivityView: UIActivityIndicatorView!
    
    // MARK: - Attributes
    
    let presenter: CharacterListPresenterProtocol
    private var characters: [CharacterViewModel] = []
    
    init(presenter: CharacterListPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: "CharacterListViewController", bundle: .main)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        presenter.viewDidLoad()
    }
}

extension CharacterListViewController: CharacterListView {
    
    func showLoading() {
        searchBarActivityView.startAnimating()
    }
    
    func dismissLoading() {
        searchBarActivityView.stopAnimating()
    }
    
    func showPaginationLoading() {
        paginationView.isHidden = false
        paginationActivityView.startAnimating()
    }
    
    func dismissPaginationLoading() {
        paginationView.isHidden = true
        paginationActivityView.stopAnimating()
    }
    
    func showCharacters(_ characters: [CharacterViewModel]) {
        self.characters = characters
        tableView.reloadData()
        guard characters.count > 0 else { return }
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
    
    func addCharacters(_ characters: [CharacterViewModel]) {
        self.characters += characters
        tableView.beginUpdates()
        let indexPaths: [IndexPath] = self.characters.enumerated().reduce(into: []) { newIndexPaths, item in
            guard characters.contains(item.element) else { return }
            newIndexPaths.append(IndexPath(row: item.offset, section: 0))
        }
        tableView.insertRows(at: indexPaths, with: .bottom)
        tableView.endUpdates()
    }
}

extension CharacterListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        defer {
            pagination(forIndexPath: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterTableViewCell", for: indexPath) as? CharacterTableViewCell
        cell?.setupWithViewModel(characters[indexPath.row])
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return characters.count
    }
}

extension CharacterListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectCharacter(characters[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension CharacterListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.didSearch(term: searchText)
    }
}

private extension CharacterListViewController {
    
    func pagination(forIndexPath indexPath: IndexPath) {
        guard presenter.isPaginationEnabled, characters.count - 1 == indexPath.row else { return }
        presenter.loadMoreCharacters()
    }
    
    func setupView() {
        title = "Character List"
        setupTableView()
    }
    
    func setupTableView() {
        tableView.register(UINib(nibName: "CharacterTableViewCell", bundle: .main), forCellReuseIdentifier: "CharacterTableViewCell")
    }
}
