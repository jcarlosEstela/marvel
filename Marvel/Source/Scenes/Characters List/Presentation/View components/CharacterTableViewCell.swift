//
//  CharacterTableViewCell.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 09/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var characterImageView: URLImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var nameLabelContainerView: UIView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        characterImageView.image = nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    func setupWithViewModel(_ viewModel: CharacterViewModel) {
        nameLabel.text = viewModel.name
        characterImageView.setImage(fromUrl: viewModel.thumbnail)
    }
}

extension CharacterTableViewCell {
    
    func setupView() {
        characterImageView.contentMode = .scaleAspectFill
        nameLabelContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        nameLabel.font = .boldSystemFont(ofSize: 16)
        nameLabel.textColor = .white
    }
}
