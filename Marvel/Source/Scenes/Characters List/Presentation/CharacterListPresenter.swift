//
//  CharacterListPresenter.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 07/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

protocol CharacterListPresenterProtocol {
    var view: CharacterListView? { get }
    var isPaginationEnabled: Bool { get }
    func viewDidLoad()
    func didSearch(term: String?)
    func loadMoreCharacters()
    func didSelectCharacter(_ character: CharacterViewModel)
}

final class CharacterListPresenter {
    
    private struct Search {
        let offset: Int
        let term: String?
    }
    
    private let loadCharactersUseCase: LoadCharactersUseCaseProtocol
    private let loadCharactersByNameUseCase: LoadCharactersByNameUseCaseProtocol
    private var currentSearch: Search?
    private lazy var strategyFactory: LoadCharactersUseCaseStrategyFactory = {
       return LoadCharactersUseCaseStrategyFactory(loadAllCharactersUseCase: loadCharactersUseCase, loadCharactersByNameUseCase: loadCharactersByNameUseCase)
    }()
    private let coordinator: CharacterListCoordinatorProtocol
    weak var view: CharacterListView?
    var isPaginationEnabled: Bool = false
    
    init(loadCharactersUseCase: LoadCharactersUseCaseProtocol, loadCharactersByNameUseCase: LoadCharactersByNameUseCaseProtocol, coordinator: CharacterListCoordinatorProtocol) {
        self.loadCharactersUseCase = loadCharactersUseCase
        self.loadCharactersByNameUseCase = loadCharactersByNameUseCase
        self.coordinator = coordinator
    }
}

extension CharacterListPresenter: CharacterListPresenterProtocol {
    
    func viewDidLoad() {
        didSearch(term: nil)
    }
    
    func didSearch(term: String?) {
        isPaginationEnabled = false
        view?.showLoading()
        loadCharacters(withTerm: term) { [weak self] result in
            self?.handleSearchResult(result, term: term)
        }
    }
    
    func loadMoreCharacters() {
        isPaginationEnabled = false
        let currentOffset = offset(forTerm: currentSearch?.term)
        view?.showPaginationLoading()
        loadCharacters(withTerm: currentSearch?.term, offset: currentOffset) { [weak self] result in
            self?.handleLoadMoreCharactersResult(result, offset: currentOffset)
        }
    }
    
    func didSelectCharacter(_ character: CharacterViewModel) {
        coordinator.goToCharacterDetail(character)
    }
}

private extension CharacterListPresenter {
    
    func handleSearchResult(_ result: Result<LoadCharactersUseCaseOutputProtocol, Error>, term: String?) {
        switch result {
        case .success(let output):
            isPaginationEnabled = output.isPaginationEnabled
            currentSearch = Search(offset: output.characters.count, term: term)
            let characterViewModels = output.characters.map(CharacterViewModel.init)
            view?.showCharacters(characterViewModels)
        case .failure(let error):
            showError(error)
        }
        view?.dismissLoading()
    }
    
    func handleLoadMoreCharactersResult(_ result: Result<LoadCharactersUseCaseOutputProtocol, Error>, offset: Int) {
        switch result {
        case .success(let output):
            isPaginationEnabled = output.isPaginationEnabled
            currentSearch = Search(offset: offset + output.characters.count, term: self.currentSearch?.term)
            let characterViewModels = output.characters.map(CharacterViewModel.init)
            view?.addCharacters(characterViewModels)
        case .failure(let error):
            showError(error)
        }
        view?.dismissPaginationLoading()
    }
    
    func showError(_ error: Error) {
        view?.showAlert(
            title: "Error",
            description: error.localizedDescription,
            accept: AlertAction(title: "OK"),
            cancel: nil
        )
    }
    
    func loadCharacters(withTerm term: String?, offset: Int = 0, completion: @escaping (Result<LoadCharactersUseCaseOutputProtocol, Error>) -> Void) {
        let strategy = strategyFactory.get(withTerm: term, offset: offset)
        strategy.loadCharacters(completion: completion)
    }
    
    func offset(forTerm term: String?) -> Int {
        guard currentSearch?.term == term else { return 0 }
        return currentSearch?.offset ?? 0
    }
}
