//
//  CharacterListCoordinator.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 08/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import UIKit

protocol CharacterListCoordinatorProtocol {
    func goToCharacterDetail(_ character: CharacterViewModel)
}

final class CharacterListCoordinator: Coordinator {
    
    weak var navigationController: UINavigationController?
    var detailCoordinator: CharacterDetailCoordinator?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let repository = LoadCharactersAPIRepository(restClient: DefaultRestClient())
        let presenter = CharacterListPresenter(loadCharactersUseCase: LoadCharactersUseCase(repository: repository), loadCharactersByNameUseCase: LoadCharactersByNameUseCase(repository: repository), coordinator: self)
        let view = CharacterListViewController(presenter: presenter)
        presenter.view = view
        self.navigationController?.setViewControllers([view], animated: false)
    }
}

extension CharacterListCoordinator: CharacterListCoordinatorProtocol {
    
    func goToCharacterDetail(_ character: CharacterViewModel) {
        detailCoordinator = CharacterDetailCoordinator(navigationController: navigationController, character: character)
        detailCoordinator?.start()
    }
}
