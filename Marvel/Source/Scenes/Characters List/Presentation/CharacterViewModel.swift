//
//  CharacterViewModel.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 08/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct CharacterViewModel {
    
    let entity: Character
    
    var name: String {
        entity.name
    }
    
    var thumbnail: String {
        entity.thumbnail
    }
    
    var comics: [String] {
        entity.comics.map { $0.name }
    }
    
    var stories: [String] {
        entity.stories.map { $0.name }
    }
}

extension CharacterViewModel: Equatable {
    
    static func == (lhs: CharacterViewModel, rhs: CharacterViewModel) -> Bool {
        return lhs.entity == rhs.entity
    }
}

