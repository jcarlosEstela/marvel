//
//  LoadCharacterAPIRepository.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 05/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

final class LoadCharactersAPIRepository: LoadCharactersRepository {
    
    private let restClient: RestClient
    
    init(restClient: RestClient) {
        self.restClient = restClient
    }
    
    func get(from offset: Int, limit: Int, completion: @escaping (Result<[Character], Error>) -> Void) {
        fetchCharacters(from: offset, limit: limit) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let characterList):
                let characters = characterList.data.results.map { Character(dto: $0) }
                completion(.success(characters))
            }
        }
    }
    
    func get(nameStartsWith: String, from offset: Int, limit: Int, completion: @escaping (Result<[Character], Error>) -> Void) {
        fetchCharacters(nameStartsWith: nameStartsWith, from: offset, limit: limit) { result in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let characterList):
                let characters = characterList.data.results.map { Character(dto: $0) }
                completion(.success(characters))
            }
        }
    }
}

extension LoadCharactersAPIRepository: MarvelRepository {}

private extension LoadCharactersAPIRepository {
    
    func fetchCharacters(nameStartsWith: String, from offset: Int, limit: Int, completion: @escaping (Result<CharacterListDTO, Error>) -> Void) {
        let timestamp = Date().timeIntervalSince1970.description
        let apikey = Constants.Api.Authentication.key
        restClient.request(
            url: Constants.Api.baseUrl + "characters",
            method: .get,
            query: LoadCharactersByNameRequestParamsDTO(
                apikey: apikey,
                timestamp: timestamp,
                hash: hash(withTimestamp: timestamp),
                offset: String(offset),
                limit: String(limit),
                nameStartsWith: nameStartsWith
            ),
            completion: completion
        )
    }
    
    func fetchCharacters(from offset: Int, limit: Int, completion: @escaping (Result<CharacterListDTO, Error>) -> Void) {
        let timestamp = Date().timeIntervalSince1970.description
        let apikey = Constants.Api.Authentication.key
        restClient.request(
            url: Constants.Api.baseUrl + "characters",
            method: .get,
            query: LoadCharactersRequestParamsDTO(
                apikey: apikey,
                timestamp: timestamp,
                hash: hash(withTimestamp: timestamp),
                offset: String(offset),
                limit: String(limit)
            ),
            completion: completion
        )
    }
}

private extension Character {
    
    init(dto: CharacterDTO) {
        self.name = dto.name
        self.id = dto.id
        self.thumbnail = dto.thumbnail.path + "." + dto.thumbnail.thumbnailExtension
        self.comics = dto.comics.items.map({ Comic(name: $0.name) })
        self.stories = dto.stories.items.map({ Story(name: $0.name) })
    }
}
