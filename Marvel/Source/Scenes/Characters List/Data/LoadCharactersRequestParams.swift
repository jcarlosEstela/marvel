//
//  LoadCharactersRequestParams.swift
//  Marvel
//
//  Created by José Carlos Estela Anguita on 07/11/2020.
//  Copyright © 2020 JEST. All rights reserved.
//

import Foundation

struct LoadCharactersRequestParamsDTO: Encodable {
    let apikey: String
    let timestamp: String
    let hash: String
    let offset: String
    let limit: String
    
    enum CodingKeys: String, CodingKey {
        case apikey, hash, offset, limit
        case timestamp = "ts"
    }
}

struct LoadCharactersByNameRequestParamsDTO: Encodable {
    let apikey: String
    let timestamp: String
    let hash: String
    let offset: String
    let limit: String
    let nameStartsWith: String
    
    enum CodingKeys: String, CodingKey {
        case apikey, hash, offset, limit, nameStartsWith
        case timestamp = "ts"
    }
}
