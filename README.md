# Marvel app

## Summary

The app is a Master-Detail app with a simply interface.

## Installation

The app has `CocoaPods` as dependency manager. So, before opening the project, execute the following command:

`pod install`

And then, open the `Marvel.xcworkspace`.

## Dependency Injection

The app uses constructor dependency injection.

## Architecture

The app architecture is MVP + Clean Architecture with the following layers:

* View (ViewController)
* Coordinator
* Presenter
* Use Cases
* Repositories

## Interface

I have used `xibs` to create the views of the app. But, in the "Character Detail" view, I added all subviews programmatically in order to show the two ways that I usually work.

## Testing

The app has some behaviour tests in the "Character List" scene.

## External tools 

I have used some external tools to improve the development flow:

* Postman (to test the API and get the response)
* Paste JSON as Code * quicktype (to get the Swift entities from json)
* InjectionIII (to modify code without re-compile)
* Sherlock (to inspect and modify the view in runtime)
